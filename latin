"Nemo enim fere saltat sobrius, nisi forte insanit, neque in solitudine
neque in convivio moderato atque honesto."
		-- M. Tulli Ciceronis Pro Murena Oratio
%
"Non nobis solum nati sumus ortusque nostri partem patria vindicat,
partem amici."
		-- M. Tulli Ciceronis De Officiis
%
"Sed tamen ira procul absit, cum qua nihil recte fieri nec considerate
potest."
		-- M. Tulli Ciceronis De Officiis
%
"Nam et secundas res splendidiores facit amicitia et adversas partiens
communicansque leviores."
		-- M. Tulli Ciceronis Laelius De Amicitia
%
"... Quod nisi idem in amicitiam transferetur, verus amicus numquam
reperietur; est enim is qui est tamquam alter idem."
		-- M. Tulli Ciceronis Laelius De Amicitia
%
"Nam efficit hoc philosophia: medetur animis, inanes sollicitudines
detrahit, cupiditatibus liberat, pellit timores."
		-- M. Tulli Ciceronis Tusculanae Disputationes
%
"Est profecto animi medicina philosophia; cuius auxilium non ut in
corporis morbis petendum est foris, omnibusque opibus viribus, ut nosmet
ipsi nobis mederi possimus, elaborandum est."
		-- M. Tulli Ciceronis Tusculanae Disputationes
%
"O passi graviora, dabit deus his quoque finem!"
		-- P. Vergili Maronis Aeneidos Liber I
%
"Revocate animos, maestumque timorem
mittite: forsan et haec olim meminisse iuvabit."
		-- P. Vergili Maronis Aeneidos Liber I
%
"Hunc igitur terrorem animi tenebrasque necessest
non radii solis neque lucida tela diei
discutiant, sed naturae species ratioque.
Principium cuius hinc nobis exordia sumet,
nullam rem e nihilo gigni divinitus umquam."
		-- T. Lucreti Cari De Rerum Natura
%
"Nam si certam finem esse viderent
aerumnarum homines, aliqua ratione valerent
religionibus atque minis obsistere vatum."
		-- T. Lucreti Cari De Rerum Natura
%
"Nil igitur mors est ad nos neque pertinet hilum,
quandoquidem natura animi mortalis habetur."
		-- T. Lucreti Cari De Rerum Natura
%
"Nec prorsum vitam ducendo demimus hilum
tempore de mortis nec delibare valemus."
		-- T. Lucreti Cari De Rerum Natura
%
"Circumretit enim vis atque iniuria quemque,
atque, unde exortast, at eum plerumque revertit."
		-- T. Lucreti Cari De Rerum Natura
%
"Quod siquis vera vitam ratione gubernet,
divitiae grandes homini sunt vivere parvo
aequo animo; neque enim est umquam penuria parvi."
		-- T. Lucreti Cari De Rerum Natura
%
"Omnes homines, patres conscripti, qui de rebus dubiis consultant, ab
odio, amicitia, ira atque misericordia vacuos esse decet."
		-- C. Sallusti Crispi Bellum Catilinae
%
"Quis fuit, horrendos primus qui protulit enses?
quam ferus et vere ferreus ille fuit!"
		-- Albi Tibulli Elegiae
%
"Nil ego contulerim iucundo sanus amico."
		-- Q. Horati Flacci Sermones
%
"Ut melius quicquid erit pati,
seu pluris hiemes, seu tribuit Iuppiter ultimam,
quae nunc oppositis debilitat pumicibus mare
Tyrrhenum."
		-- Q. Horati Flacci Carmina
%
"Sapias, vina liques, et spatio brevi
spem longam reseces."
		-- Q. Horati Flacci Carmina
%
"Dum loquimur, fugerit invida
aetas. Carpe diem, quam minimum credula postero."
		-- Q. Horati Flacci Carmina
%
"Auream quisquis mediocritatem
diligit, tutus caret obsoleti
sordibus tecti, caret invidenda
sobrius aula."
		-- Q. Horati Flacci Carmina
%
"Aequam memento rebus in arduis
servare mentem."
		-- Q. Horati Flacci Carmina
%
"Eheu fugaces, Postume, Postume,
labuntur anni, nec pietas moram
rugis et instanti senectae
adferet indomitaeque morti."
		-- Q. Horati Flacci Carmina
%
"Frustra cruento Marte carebimus
fractisque rauci fluctibus Hadriae;
frustra per autumnos nocentem
corporibus metuemus Austrum.
Visendus ater flumine languido
Cocytos errans et Danai genus
infame damnatusque longi
Sisyphus Aeolides laboris."
		-- Q. Horati Flacci Carmina
%
"Quid sit futurum cras fuge quaerere et
quem Fors dierum cumque dabit lucro
appone, nec dulcis amores
sperne puer neque tu choreas, 
donec virenti canities abest
morosa."
		-- Q. Horati Flacci Carmina
%
"Ira furor brevis est; animum rege, qui nisi paret,
imperat, hunc frenis, hunc tu compesce catena."
		-- Q. Horati Flacci Epistulae
%
"Non est, crede mihi, sapientis dicere 'vivam':
Sera nimis vita est crastina: vive hodie."
		-- M. Valeri Martialis Epigrammata
%
"Semper eris pauper, si pauper es, Aemiliane;
Dantur opes nulli nunc, nisi divitibus."
		-- M. Valeri Martialis Epigrammata
%
"Non est paupertas, Nestor, habere nihil."
		-- M. Valeri Martialis Epigrammata
%
"Lupus est homo homini, non homo, quom qualis sit non novit."
		-- T. Plauti Asinaria
%
"Nil tam difficile est quin quaerendo investigari possiet."
		-- P. Terenti Afri Heautontimorumenos
%
"Ius summum saepe summa est malitia."
		-- P. Terenti Afri Heautontimorumenos
%
"Modo liceat vivere, est spes."
		-- P. Terenti Afri Heautontimorumenos
%
"... Denique inspicere tamquam in speculum in uitas omnium iubeo atque
ex aliis sumere exemplum sibi."
		-- P. Terenti Afri Adelphoe
%
"... Nam qui dabat olim
imperium, fasces, legiones, omnia, nunc se
continet atque duas tantum res anxius optat,
panem et circenses."
		-- D. Iuni Iuvenalis Saturae
%
"Haut facile emergunt quorum virtutibus opstat
res angusta domi."
		-- D. Iuni Iuvenalis Saturae
%
"Hic animum sensusque meos abducta relinquo,
     arbitrio quamvis non sinis esse meo."
                -- Sulpiciae Epistulae
%
"Idque apud imperitos humanitas vocabatur, cum pars servitutis esset."
		-- Corneli Taciti Agricola
%
"Auferre, trucidare, rapere, falsis nominibus imperium, atque, ubi
solitudinem faciunt, pacem appellant."
		-- Corneli Taciti Agricola
%
"Corruptissima re publica plurimae leges."
		-- Corneli Taciti Annales
