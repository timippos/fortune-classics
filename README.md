fortune-classics
================

A collection of quotes in Ancient Greek and Latin for use with the `fortune` program.
